﻿
#include <iostream>
#include <thread>
#include "yuvCodek.h"
#pragma warning(disable : 4996)	//проблемы с fopen

using namespace std;

int main()
{
	//Работа с bmp файлом
	string bmp_file;
	cout << ">>> Enter bmp file: ";
	cin >> bmp_file;

	FILE* fp = fopen(bmp_file.c_str(), "rb");

	unsigned char info[54];										//создаем массив куда помешаем структуры файла bmp
	fread(info, sizeof(unsigned char), 54, fp);					//считываем 54 байта заголовка

	//извлекаем высоту и длину картинки из заголовка
	int width = *(int*)& info[18];
	int height = *(int*)& info[22];

	int size = 3 * width * height;								//размер bmp файла без заголовка

	unsigned char* data = new unsigned char[size];
	fread(data, sizeof(unsigned char), size, fp);				//прочитайте остальные данные сразу
	fclose(fp);

	//Преобразование осей координат, чтобы отзеркалить картинку
	unsigned char* rgb = new unsigned char[size];
	int ind = 0;												//index массива
	int stop_ind = size;										//задает конец строки
	int start_ind = stop_ind - width * 3;						//задает начало строки

	for (int x = 0; x < height; x++) {
		for (int y = start_ind; y < stop_ind; y++) {
			rgb[ind] = data[y];
			ind++;
		}
		stop_ind = start_ind;
		start_ind = start_ind - width * 3;
	}
	delete[] data;

	//открываем формат YUV420
	string yuv_file;
	cout << ">>> Enter YUV420 file: ";
	cin >> yuv_file;

	FILE* fp2 = fopen(yuv_file.c_str(), "r+b");
	int width_yuv;
	int heigth_yuv;
	int frames;

	//Ввод данных
	cout << ">>> Enter YUV420 parametrs: " << endl;
	cout << ">>> Width and Heigth: " << endl << ">>> ";
	cin >> width_yuv >> heigth_yuv;
	cout << ">>> Frames: ";
	cin >> frames;

	int size_y = width_yuv * heigth_yuv * 3 / 2;				//размер файла 1 фрейма

	cout << endl << ">>> Conversion started" << endl;
	//преобразование RGB в YVU
	unsigned char* yvu = new unsigned char[size];

	thread stream_y(to_yvu, std::ref(yvu), rgb, size, 0, 66, 129, 25, 16);		//вычисление Y
	thread stream_v(to_yvu, std::ref(yvu), rgb, size, 1, 112, -94, -18, 128);	//вычисление V
	thread stream_u(to_yvu, std::ref(yvu), rgb, size, 2, -38, -74, 112, 128);	//вычисление U

	stream_y.join();
	stream_v.join();
	stream_u.join();
	delete[] rgb;

	//преобразование в структуру YUV420
	unsigned char* yvu_struct = new unsigned char[size_y];			//здесь будет итоговая структура YUV420

	int max_ind_y = size_y / 1.5;									//индекс последнего элемента Y, и начального элемента V
	int max_ind_v = size_y / 6 + size_y / 1.5;						//индекс последнего элемента V, и начального элемента U 
	int max_ind_u = size_y;											//индекс последнего элемента U

	thread stream1(y_struct, yvu, std::ref(yvu_struct), width,		//добавление Y
		height, width_yuv, max_ind_y, size_y);
	thread stream2(vu_struct, yvu, std::ref(yvu_struct), width,		//добавление V
		height, width_yuv, max_ind_y, max_ind_v, size_y, 1);
	thread stream3(vu_struct, yvu, std::ref(yvu_struct), width,		//добавление U
		height, width_yuv, max_ind_v, max_ind_u, size_y, 2);

	stream1.join();
	stream2.join();
	stream3.join();
	delete[] yvu;

	cout << ">>> Conversion ended" << endl;

	//Запись поверх каждого кадра видеоряда картинки
	cout << ">>> Writing..." << endl;
	for (int i = 0; i < frames; i++) {
		fwrite(yvu_struct, sizeof(unsigned char), size_y, fp2);
	}

	fclose(fp2);
	delete[] yvu_struct;

	//Вывод информации на консоль
	cout << endl << ">>> Conversion completed" << endl;
	cout << endl << ">>> Image size:     " << size << endl;
	cout << ">>> Size of YVU420: " << size_y * frames << endl;

	return 0;
}