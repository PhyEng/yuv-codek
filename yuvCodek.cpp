#include "yuvCodek.h"
#include <iostream>
#include <thread>

using namespace std;

//Преобразование RGB в YUV
void to_yvu(unsigned char*& yvu, unsigned char* rgb, int size, 
	int index, int kf1, int kf2, int kf3, int kf4)
{	
	for (int i = 0; i < size; i += 3) {
		yvu[i + index] = (((kf1 * rgb[i]) + (kf2 * rgb[i + 1]) + (kf3 * rgb[i + 2]) + 128) >> 8) + kf4;
	}
}


///Функция для плоскости Y
void y_struct(unsigned char* yvu, unsigned char*& yvu_struct,
	int width, int height, int width_yuv, int max_ind, int size_y)
{
	int ind = 0;
	int size_rgb = 3 * width * height;

	//Если размер bmp меньше чем yuv
	//Значениями Y, соответствующие каждому отдельному пикселю 
	if (size_y * 2 > size_rgb) {
		int w_comp = width_yuv * 3 - width * 3;
		int zero_counter = 0;
		for (int i = 0; i < size_rgb; i += 3) {
			yvu_struct[ind] = yvu[i];
			ind++;
			if (((ind - zero_counter) * 3) % (width * 3) == 0) {
				for (int zeros = 0; zeros < (w_comp / 3); zeros++) {
					yvu_struct[ind] = 0;
					ind++;
					zero_counter++;
				}
			}
		}
		while (ind < max_ind) {
			yvu_struct[ind] = 0;
			ind++;
		}
	}
	//Если размер bmp равен чем yuv
	else {
		for (int i = 0; i < size_rgb; i += 3) {
			yvu_struct[ind] = yvu[i];
			ind++;
		}
	}
}


///Функция для плоскости V и U
void vu_struct(unsigned char* yvu, unsigned char*& yvu_struct,
	int width, int height, int width_yuv, int ind, int max_ind, int size_y, int correct)
{
	int size_rgb = 3 * width * height;
	int start_ind = correct;
	int stop_ind = width * 3;

	//Если размер bmp меньше чем yuv
	if (size_y * 2 > size_rgb) {
		int w_comp = width_yuv * 3 - width * 3;
		for (int i = 0; i < height / 2; i++) {
			int displacer = correct;
			for (int j = start_ind; j < stop_ind; j += 6) {
				// Добавляем U и V, находя сред ариф из соседних блоков 2*2
				if (j + 3 <= stop_ind) {
					yvu_struct[ind] = (yvu[j] + yvu[j + 3] + yvu[stop_ind + displacer] + yvu[stop_ind + displacer + 3]) / 4;
				}
				else {
					yvu_struct[ind] = (yvu[j] + yvu[stop_ind + displacer]) / 2;
				}
				ind++;
				displacer += 6;
			}
			for (int zeros = 0; zeros < (w_comp / 6); zeros++) {
				yvu_struct[ind] = 128;
				ind++;
			}
			start_ind = stop_ind + width * 3 + correct;
			stop_ind = start_ind + width * 3 - correct;
		}
		while (ind < max_ind) {
			yvu_struct[ind] = 128;
			ind++;
		}
	}
	//Если размер bmp равен чем yuv
	else {
		//Значения U и V соответствуют каждому 2*2 блоку изображения
		for (int i = 0; i < height / 2; i++) {
			int displacer = correct;
			for (int j = start_ind; j < stop_ind; j += 6) {
				if (j + 3 <= stop_ind) {
					yvu_struct[ind] = (yvu[j] + yvu[j + 3] + yvu[stop_ind + displacer] + yvu[stop_ind + displacer + 3]) / 4;
				}
				else {
					yvu_struct[ind] = (yvu[j] + yvu[stop_ind + displacer]) / 2;
				}
				ind++;
				displacer += 6;
			}
			start_ind = stop_ind + width * 3 + correct;
			stop_ind = start_ind + width * 3 - correct;
		}
	}
}
