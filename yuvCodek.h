#pragma once

void to_yvu(unsigned char*& yvu, unsigned char* rgb, int size, int index,
	int kf1, int kf2, int kf3, int kf4);

void y_struct(unsigned char* yvu, unsigned char*& yvu_struct,
	int width, int height, int width_yuv, int max_ind, int size_y);

void vu_struct(unsigned char* yvu, unsigned char*& yvu_struct,
	int width, int height, int width_yuv, int ind, int max_ind, int size_y, int correct);
